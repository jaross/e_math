Embedded Math
======

This embedded math library, libe\_math is a subset of the ANSI C math library
for single-precision floating point intended for 32-bit microarchitectures with
limited resources such as embedded processors. The code is written assuming
32-bit integer support, bit manipulation, and basic 32-bit floating point
instructions (no division or transcendentals)

[./src](./src) contains the math functions

[./test](./test) includes test codes

Current development uses the COPRTHR-2 interface for the Adapteva Epiphany
microarchitecture within the Parallella platform, but none of this should be
required to build the code. See References.

Using
-----

You may type `make` to build all directories, or `make [src|test]` to build
individual subdirectories. Running `make run` will run the test codes. There is
no installation, but you should link to the `libe_math.a` library and use the
appropriate include path for `e_math.h`.

References
-----

[COPRTHR-2 SDK](http://www.browndeertechnology.com/coprthr2.htm) -- The library is
free for non-commercial use.

[Parallella board](http://parallella.org) includes the
[Adapteva](http://adapteva.com) Epiphany microarchitecture
