float e_logf(float x)
{
	int e = *(unsigned int*)&x >> 23;
	unsigned int xe = e << 23;
	*(unsigned int*)&x ^= xe;
	*(unsigned int*)&x |= 0x3f000000;
	e -= 0x7e;

	int tmp = *(int*)&x - 0x3f3504f3;
	unsigned int i = *(unsigned int*)&tmp >> 31;
	e -= i;
	x = (i + 1)*x - 1.0f;

	float z = x * x;
	x += (((((((((7.0376836292e-2f * x
		- 1.1514610310e-1f) * x
		+ 1.1676998740e-1f) * x
		- 1.2420140846e-1f) * x
		+ 1.4249322787e-1f) * x
		- 1.6668057665e-1f) * x
		+ 2.0000714765e-1f) * x
		- 2.4999993993e-1f) * x
		+ 3.3333331174e-1f) * x
		- 0.5f) * z;
	x += 6.93147181e-1f * e;

	return x;
}
