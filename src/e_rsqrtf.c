float e_rsqrtf(float a)
{
	const float c32 = 1.5f;
	float x2 = a * 0.5f;
	float y  = a;
	long i  = * ( long * ) &y;
	i  = 0x5f3759df - ( i >> 1 );
	y  = * ( float * ) &i;
	y  = y * ( c32 - ( x2 * y * y ) ); // 1st iteration
	a  = y * ( c32 - ( x2 * y * y ) ); // 2nd iteration

	return a;
}
