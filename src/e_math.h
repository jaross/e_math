#ifndef M_PI
#define M_PI 3.1415926535f
#endif
#ifndef M_1_PI
#define M_1_PI 0.3183098861f
#endif
#ifndef M_PI_2
#define M_PI_2 1.5707963267f
#endif
#ifndef M_2_PI
#define M_2_PI 6.2831853071f
#endif

float e_cosf(float x);
float e_expf(float x);
float e_logf(float x);
float e_rsqrtf(float x);
