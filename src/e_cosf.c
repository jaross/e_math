#include "e_math.h"

float e_cosf(float x)
{
	static float C0 = 1.27323954473516f; // 4/pi
	static float C1 = 0.78515625f; // ~pi/4
	static float C2 = 2.4187564849853515625e-4f;
	static float C3 = 3.77489497744594108e-8f;

	int j = C0 * x; // integer part of x/PIO4
	j += (j & 1); // integer and fractional part modulo one octant
	float y = j;

	// Extended precision modular arithmetic
	x = ((x - y * C1) - y * C2) - y * C3;

	float z = x * x;

	if (j & 2) {
		y = (((1.9515295891e-4f * z
		     - 8.3321608736e-3f) * z
		     + 1.6666654611e-1f) * z
		     - 1.0f) * x;
	} else {
		y = (((2.443315711809948e-5f * z
		     - 1.388731625493765e-3f) * z
		     + 4.166664568298827e-2f) * z
		     - 0.5f) * z
		     + 1.0f;
	}
	y = (j & 4) ? -y : y;
	return y;
}
