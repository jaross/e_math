static float e_ldexpf(float m, int e)
{
	union { float f; int i;} r;
	r.f = m;
	r.i += (e << 23);
	return r.f;
}

static float e_floorf(float x)
{
	return (int)x;
}

float e_expf(float x)
{
	static float LOG2EF = 1.44269504089f;
	static float C = 0.693147181f;
	float z = e_floorf( LOG2EF * x + 0.5f ); // floor() truncates toward -infinity
	x -= C * z;
	int n = z;

	// Theoretical peak relative error in [-0.5, +0.5] is 4.2e-9
	z = 1.0f + x +
	((((( 1.9875691500e-4f  * x
	    + 1.3981999507e-3f) * x
	    + 8.3334519073e-3f) * x
	    + 4.1665795894e-2f) * x
	    + 1.6666665459e-1f) * x
	    + 5.0000001201e-1f) * x * x;

	x = e_ldexpf( z, n ); // multiply by power of 2
	return x ;
}
