SUBDIRS = src test

.PHONY: all $(SUBDIRS) clean distclean

all: $(SUBDIRS)
	for d in $(SUBDIRS); do \
		$(MAKE) -C $$d; \
	done

src:
	$(MAKE) -C src

# dependencies on libe_math
test: src

clean:
	for d in $(SUBDIRS); do \
		$(MAKE) -C $$d clean; \
	done

distclean:
	for d in $(SUBDIRS); do \
		$(MAKE) -C $$d distclean; \
	done
