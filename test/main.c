#ifdef __epiphany__
#include <host_stdio.h>
#define printf(...) host_printf(__VA_ARGS__)
#else
#include <stdio.h>
#include <math.h>
#endif

#include "e_math.h"

#define N 100

int main(void)
{
	int i;
	float sum_err = 0.0f;
	for (i = 0; i <= N; i++) {
		float a = -3*M_PI + 6*M_PI*((float)i)/((float)N);
		float f0 = cosf(a);
		float f1 = e_cosf(a);
		float a0 = fabs(f0);
		float a1 = fabs(f1);
		float err = (f0 - f1) / f0 * 100.0f;
		sum_err += fabs(err);
		int i0 = *((int*)&a0);
		int i1 = *((int*)&a1);
		int de = abs(i0 - i1);
		printf("% 2.8f: % 2.8f % 2.8f (% 1.6f%% %8d)\n", a, f0, f1, err, de);
	}
	printf("Average Error: %f%%\n", sum_err / ((float)N));
	return 0;
}
